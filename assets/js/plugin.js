jQuery(document).ready(function ($) {
  lazyLoad();

  swiperInit({
    className: ".main_slider",
    breakpoints: false,
    observer: true,
    observeParents: true,
  });

  swiperInit({
    className: ".location_slider__",
    breakpoints: false,
    observer: true,
    observeParents: true,
  });

  $(".product_card").fancybox({
    caption: function (instance, item) {
      var caption = $(this).data("caption") || "";

      if (item.type === "image") {
        caption =
          (caption.length ? caption + "<br />" : "") +
          `<h3 class="fancybox_title__">${this.querySelectorAll('.title__')[0].innerHTML}</h3>` +
          `<p class="fancybox_parag__">${this.querySelectorAll('.parag__')[0].innerHTML}</p>` +
          `<h5 class="fancybox_price">${this.querySelectorAll('.price')[0].innerHTML}</h5>`;
      }

      return caption;
    },
  });

  stickyHeader($);
  showPassword($);
  collapseFooterMenusInSmallScreens($);
  toggleSideMenuInSmallScreens($);
  verificationCodeSeprate();
  selectPIckerInit($);
  productsLayout($);
  createElement();
});

// functions init
function selectPIckerInit($) {
  $(".selectpicker").selectpicker();
}

function lazyLoad() {
  const images = document.querySelectorAll(".lazy-omd");

  const optionsLazyLoad = {
    //  rootMargin: '-50px',
    // threshold: 1
  };

  const preloadImage = function (img) {
    img.src = img.getAttribute("data-src");
    img.onload = function () {
      img.parentElement.classList.remove("loading-omd");
      img.parentElement.classList.add("loaded-omd");
      img.parentElement.parentElement.classList.add("lazy-head-om");
    };
  };

  const imageObserver = new IntersectionObserver(function (enteries) {
    enteries.forEach(function (entery) {
      if (!entery.isIntersecting) {
        return;
      } else {
        preloadImage(entery.target);
        imageObserver.unobserve(entery.target);
      }
    });
  }, optionsLazyLoad);

  images.forEach(function (image) {
    imageObserver.observe(image);
  });
}

function swiperInit(options) {
  // console.log(options);
  const swiper = new Swiper(options.className + " .swiper-container", {
    spaceBetween: 30,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
    },
    rtl: $("html").attr("dir") === "rtl" ? true : false,
    pagination: {
      el: options.className + " .swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: options.className + " .swiper-button-next",
      prevEl: options.className + " .swiper-button-prev",
    },
    breakpoints: options.breakpoints,
    observer: options.observer,
    observeParents: options.observeParents,
    grid: options.grid,
    ...options,
  });

  lazyLoad();

  return swiper;
}

function verificationCodeSeprate() {
  const inputElements = [...document.querySelectorAll("input.code-input")];

  inputElements.forEach((ele, index) => {
    ele.addEventListener("keydown", (e) => {
      // if the keycode is backspace & the current field is empty
      // focus the input before the current. The event then happens
      // which will clear the input before the current
      if (e.keyCode === 8 && e.target.value === "") {
        inputElements[Math.max(0, index - 1)].focus();
      }
    });
    ele.addEventListener("input", (e) => {
      if (e.target.value === "") {
        inputElements[index].classList = "code-input";
      } else {
        inputElements[index].classList = "code-input active";
      }

      // take the first character of the input
      // this actually breaks if you input an emoji like 👨‍👩‍👧‍👦....
      // but I'm willing to overlook insane security code practices.
      const [first, ...rest] = e.target.value;
      e.target.value = first ?? ""; // the `??` '' is for the backspace usecase
      const lastInputBox = index === inputElements.length - 1;
      const insertedContent = first !== undefined;
      if (insertedContent && !lastInputBox) {
        // continue to input the rest of the string
        inputElements[index + 1].focus();
        inputElements[index + 1].value = rest.join("");
        inputElements[index + 1].dispatchEvent(new Event("input"));
      }
    });
  });
}

function showPassword($) {
  $(".show-password-button-om").on("click", function (e) {
    e.preventDefault();

    if ($(this).parent().find("input").attr("type") == "text") {
      $(this).parent().find("input").attr("type", "password");
      $(this).removeClass("show-om");
    } else {
      $(this).parent().find("input").attr("type", "text");
      $(this).addClass("show-om");
    }
  });
}

function collapseFooterMenusInSmallScreens($) {
  if ($(window).width() <= 991) {
    $(".collapse-head-om").on("click", function (e) {
      e.preventDefault();

      $(".collapse-head-om")
        .not(this)
        .parent()
        .find(".list-collapse-om")
        .slideUp();
      $(this)
        .parent()
        .find(".list-collapse-om")
        .slideToggle({
          queue: false,
          complete: function () {
            $(".list-collapse-om").each(function () {
              if ($(this).css("display") == "none") {
                $(this).parent().removeClass("active");
              } else {
                $(this).parent().addClass("active");
              }
            });
          },
        });
    });
  }
}

function toggleSideMenuInSmallScreens($) {
  // nav men activation
  $("#menu-butt-activ-om").on("click", function (e) {
    e.preventDefault();

    $("#navbar-menu-om").addClass("active-menu");
    $(".overlay").addClass("active");
    $("body").addClass("overflow-body");
  });

  // nav men close
  $(".close-button__ , .overlay ").on("click", function (e) {
    e.preventDefault();
    $("#navbar-menu-om").removeClass("active-menu");
    $(".overlay").removeClass("active");

    $("body").removeClass("overflow-body");
  });
}

function stickyHeader($) {
  let lastScroll = 0;
  const fixedHeaderElement = $(".fixed_header__");

  $(document).on("scroll", function () {
    let currentScroll = $(this).scrollTop();

    const isScrollingDown = function () {
      return currentScroll < lastScroll && currentScroll > 500;
    };
    const isScrollingUp = function () {
      return currentScroll > lastScroll && currentScroll > 500;
    };

    const fixedHeightToHeaderWrapper = function (fixedHeaderElement) {
      const fixedHeaderElementHeight = fixedHeaderElement.innerHeight();
      const sliderHeaderElementHeight = $(".main_slider").height();

      $(".main_header__").css(
        "height",
        fixedHeaderElementHeight
      );

      $(".home_header__").css(
        "height",
        fixedHeaderElementHeight + sliderHeaderElementHeight
      );
    };

    fixedHeightToHeaderWrapper(fixedHeaderElement);

    if (isScrollingDown()) {
      fixedHeaderElement
        .addClass("active_menu__")
        .removeClass("not_active_menu__");
    } else if (isScrollingUp()) {
      fixedHeaderElement
        .removeClass("active_menu__")
        .addClass("not_active_menu__");
    } else {
      fixedHeaderElement.removeClass("active_menu__ not_active_menu__");
    }
    lastScroll = currentScroll;
  });
}

function productsLayout($) {
  const swiperBreakNormalPoints = {
    0: {
      slidesPerView: 2,
    },
    480: {
      slidesPerView: 2,
    },
    767: {
      slidesPerView: 3,
    },
    992: {
      slidesPerView: 3,
    },
    1200: {
      slidesPerView: 3,
    },
  };

  const swiperVerticalBreakPoints = {
    0: {
      slidesPerView: 1,
    },
    480: {
      slidesPerView: 1,
    },
    767: {
      slidesPerView: 1,
    },
    992: {
      slidesPerView: 2,
    },
    1200: {
      slidesPerView: 2,
    },
  };

  const swiperProps = {
    autoplay: false,
    slidesPerColumn: 2,
    loop: false,
    spaceBetween: 16,
    slidesPerColumnFill: "row",
  };

  swiperProps.breakpoints = swiperBreakNormalPoints;
  swiperProps.className = "#gallery_slider__";
  swiperInit(swiperProps);
}

function createElement() {
  if (!(document.querySelector('.home_header__'))) return ;
  for (i = 1; i <= 5; i++) {
    let className = `circle_shape${i}`;
    var ele = `<div class=\'${className} circle_shape__\'div>`;
    $(".home_header__").append(ele);
    ele = $(".home_header__").find(`.${className}`);
    animateDiv(ele);
  }
}

function makeNewPosition() {
  var h = $(".home_header__").height() - 50;
  var w = $(".home_header__").width() - 50;

  var nh = Math.floor(Math.random() * h);
  var nw = Math.floor(Math.random() * w);

  return [nh, nw];
}

function animateDiv(ele) {
  var newq = makeNewPosition();
  var oldq = ele.offset();
  var speed = calcSpeed([oldq.top, oldq.left], newq);

  ele.animate({ top: newq[0], left: newq[1] }, speed, function () {
    animateDiv(ele);
  });
}

function calcSpeed(prev, next) {
  var x = Math.abs(prev[1] - next[1]);
  var y = Math.abs(prev[0] - next[0]);

  var greatest = x > y ? x : y;

  var speedModifier = 0.1;

  var speed = Math.ceil(greatest / speedModifier);

  return speed;
}
